#ifndef TIMESTAMPREGISTRY_H
#define TIMESTAMPREGISTRY_H

#include <string>
#include <map>
#include "api/Task.h"
#include <sqlite3x/sqlite3x.hpp>

/**
 * Stores the time of last execution for tasks and stores it in a file backend.
 */
class TimestampRegistry {
public:
    explicit TimestampRegistry();

    std::chrono::system_clock::time_point getLastExecution(Task *task);

    void setLastExecution(Task *task, std::chrono::system_clock::time_point time);

private:
    sqlite3x::sqlite3_connection connection;

    // maps task IDs to the start time of their last execution
    //std::map<std::string, std::chrono::system_clock::time_point> lastExecution;

    std::chrono::system_clock::time_point fallbackTime;
};

#endif //TIMESTAMPREGISTRY_H
