#ifndef TASKREGISTRY_H
#define TASKREGISTRY_H

#include <vector>
#include "SchedulerListener.h"
#include "api/Task.h"
#include "utils/ConditionalThread.h"
#include "utils/Notifier.h"
#include "api/NotificationProvider.h"
#include "api/AuthenticationProvider.h"
#include "TimestampRegistry.h"

class Scheduler : public ConditionalThread, public Notifier<SchedulerListener> {
public:
    Scheduler(const std::vector<Task *> &tasks, const std::vector<Condition *> &conditions,
              const ProviderStore &providers);

    const std::vector<Task *> &getTasks() const;

    SchedulerListener::Status getStatus() const;

    void forceRun(Task *task);

private:
    ProviderStore providers;

    std::vector<Task *> tasks;
    std::vector<Condition *> conditions;

    TimestampRegistry lastTaskExecution;
    std::map<Task *, std::chrono::system_clock::time_point> nextTaskExecution;
    std::map<Condition *, std::chrono::system_clock::time_point> nextConditionUpdate;

    SchedulerListener::Status currentStatus = SchedulerListener::Status::STOPPED;
    Task *forcedTask = nullptr;

    void updateState(SchedulerListener::Status status, Task *task);

    void run() override;

    void stopImpl() override;

    /**
     * Checks all conditions in the list if they need to be updated and updates the values in `nextConditionUpdate`.
     * If force is true, the conditions will be updated independent of their last update time.
     */
    void updateConditions(const std::vector<Condition *> &conditionList, bool force = false);

    /**
     * Checks all tasks if they need to be run, runs them if necessary, and updates the values in
     * `nextTaskExecution`.
     */
    void updateTasks();

    /**
     * Checks the task's conditions and tries to run it.
     */
    void runTask(Task *task, bool showNotifications);

    std::chrono::system_clock::time_point findNextLoopTime();
};

#endif //TASKREGISTRY_H
