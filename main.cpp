#include "utils/Logger.h"
#include "api/Condition.h"
#include "Scheduler.h"
#include "ConfigParser.h"
#include "utils/SystemInfo.h"
#include "utils/Utils.h"
#include "api/AuthenticationProvider.h"
#include <nlohmann/json.hpp>
#include "gui/TrayIcon.cpp"

using namespace std;
using namespace nlohmann;

int main() {
    // start logger
    Logger::getInstance().start();
    Logger::getInstance().setLevel(Logger::Level::INFO);

    // initialise system info
    Logger::debug("Main: gathering system info...");
    SystemInfo::getInstance();

    // read config
    Logger::debug("Main: reading JSON config...");
    const string configFile = "./config.json";
    json config;
    try {
        string s = Utils::readFile(configFile);
        config = json::parse(s);
    } catch (exception &e) {
        Logger::fatal("config file '" + configFile + "' not found or not parsable: " + e.what());
        Logger::getInstance().stop();
        exit(1);
    }

    if (config.contains("debug-log") && config["debug-log"].is_boolean() && config["debug-log"] == true) {
        Logger::getInstance().setLevel(Logger::Level::DEBUG);
    }

    // parse providers
    ProviderStore providers;
    Logger::debug("parsing notification provider...");
    if (config.contains("notifications") && config["notifications"].is_object())
        providers.setNotificationProviders(ConfigParser::parseNotificationProvider(config["notifications"]));

    Logger::debug("parsing authentications...");
    if (config.contains("authentications") && config["authentications"].is_object())
        providers.setAuthenticationProvider(ConfigParser::parseAuthenticationProvider(config["authentications"]));

    // parse conditions
    Logger::debug("parsing conditions...");
    map<string, Condition *> conditions;
    if (config.contains("conditions"))
        conditions = ConfigParser::parseConditions(config["conditions"]);

    // parse tasks
    Logger::debug("parsing tasks...");
    vector<Task *> tasks;
    if (config.contains("tasks") && config["tasks"].is_object())
        tasks = ConfigParser::parseTasks(config["tasks"], conditions);

    // initial delay
    if (config.contains("initial_delay")) {
        auto delay = ConfigParser::parseInterval(config["initial_delay"]);
        Logger::info("initial delay: waiting " + to_string(delay.count()) + " seconds");
        std::this_thread::sleep_for(delay);
    }

    // create scheduler
    Scheduler scheduler(tasks, Utils::values(conditions), providers);

    // parse and creating GUI
    Logger::debug("parsing GUI...");
    void *guiPtr = nullptr;
    if (config.contains("gui") && config["gui"].is_object()) {
        guiPtr = ConfigParser::createGUI(config["gui"], scheduler);
    }

    // run scheduler
    scheduler.start();
    scheduler.join();
    Logger::info("ending main loop");
    Logger::getInstance().stop();
    //TODO: this is ugly
    delete guiPtr;

    return 0;
}
