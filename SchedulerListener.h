#ifndef SCHEDULERLISTENER_H
#define SCHEDULERLISTENER_H

#include "api/Task.h"

class SchedulerListener {
public:
    enum class Status {
        STOPPED, IDLE, RUNNING_TASK
    };

    virtual void statusChanged(Status status, Task *task) = 0;
};

#endif //SCHEDULERLISTENER_H
