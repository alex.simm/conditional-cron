#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

#include <string>
#include "VersionNumber.h"

class SystemInfo {
public:
    enum class PackageManager {
        DNF, APT, UNKNOWN
    };
    enum class Distribution {
        FEDORA, UBUNTU, ARCH_LINUX, UNKNOWN
    };

    static SystemInfo &getInstance() {
        static SystemInfo instance;
        return instance;
    }

    SystemInfo(SystemInfo const &) = delete;

    void operator=(SystemInfo const &) = delete;

    PackageManager getPackageManager() const;

    Distribution getDistribution() const;

    const VersionNumber &getDistributionVersion() const;

private:
    SystemInfo();

    PackageManager packageManager;
    Distribution distribution;
    VersionNumber distributionVersion;

    static Distribution parseDistribution(const std::string &name);
};

#endif //SYSTEMINFO_H
