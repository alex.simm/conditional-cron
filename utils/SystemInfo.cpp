#include "SystemInfo.h"
#include "Utils.h"
#include "Logger.h"

using namespace std;

SystemInfo::SystemInfo() {
    // find Linux distribution and version
    distribution = Distribution::UNKNOWN;
    distributionVersion = VersionNumber("");
    try {
        map<string, string> pairs = Utils::readKeyValueFile("/etc/os-release");
        if (Utils::contains(pairs, string("NAME"))) {
            distribution = parseDistribution(pairs["NAME"]);
        }
        if (Utils::contains(pairs, string("VERSION_ID"))) {
            distributionVersion = VersionNumber(pairs["VERSION_ID"]);
        }
    } catch (exception &e) {
    }

    // find package manager
    if (Utils::isCommandAvailable("apt-get")) {
        packageManager = PackageManager::APT;
    } else if (Utils::isCommandAvailable("dnf")) {
        packageManager = PackageManager::DNF;
    } else {
        Logger::error("package-update-task: could not find any package manager");
        packageManager = PackageManager::UNKNOWN;
    }
}

SystemInfo::Distribution SystemInfo::parseDistribution(const string &name) {
    string s = Utils::transform(name, [](unsigned char c) { return std::tolower(c); });
    if (s == "fedora") {
        return SystemInfo::Distribution::FEDORA;
    } else if (s == "ubuntu") {
        return SystemInfo::Distribution::UBUNTU;
    } else if (s == "arch linux") {
        return SystemInfo::Distribution::ARCH_LINUX;
    } else {
        return SystemInfo::Distribution::UNKNOWN;
    }
}

SystemInfo::Distribution SystemInfo::getDistribution() const {
    return distribution;
}

const VersionNumber &SystemInfo::getDistributionVersion() const {
    return distributionVersion;
}

SystemInfo::PackageManager SystemInfo::getPackageManager() const {
    return packageManager;
}
