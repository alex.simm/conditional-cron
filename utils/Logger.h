#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <fstream>
#include "ThreadBuffer.h"

class Logger : public ThreadBuffer<std::string> {
public:
    enum class Level {
        DEBUG = 0, INFO = 1, WARNING = 2, ERROR = 3, FATAL = 4
    };

    static Logger &getInstance() {
        static Logger instance;
        return instance;
    }

    Logger(Logger const &) = delete;

    ~Logger();

    void operator=(Logger const &) = delete;

    Level getLevel() const;

    void setLevel(Level level);

    bool isPrintingDate() const;

    void setPrintDate(bool print);

    static void log(Level level, const std::string &message);

    static void debug(const std::string &message);

    static void info(const std::string &message);

    static void warning(const std::string &message);

    static void error(const std::string &message);

    static void fatal(const std::string &message);

protected:
    void process(std::string *s) override;

private:
    const std::string LEVEL_NAMES[5] = {"DEBUG", "INFO", "WARNING", "ERROR", "SEVERE"};

    Level threshold;
    bool printDate;
    std::ofstream fileOut;

    Logger();

    void logImpl(Level level, const std::string &message);
};

#endif //LOGGER_H
