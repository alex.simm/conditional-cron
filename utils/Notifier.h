#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <vector>

/**
 * Abstract class to which listeners can be assigned.
 */
template<class T>
class Notifier {
public:
    void addListener(T *listener) {
        listeners.push_back(listener);
    }

    void removeListener(T *listener) {
        listeners.erase(std::remove(listeners.begin(), listeners.end(), listener), listeners.end());
    }

protected:
    const std::vector<T *> &getListeners() const {
        return listeners;
    }

private:
    std::vector<T *> listeners;
};

#endif //NOTIFIER_H
