#ifndef CONSUMER_H
#define CONSUMER_H

#include "Runnable.h"
#include "BlockingQueue.h"

/**
 * A thread that waits for elements to process. New elements can be pushed and the process function needs to be
 * overwritten.
 */
template<class T>
class ThreadBuffer : public Runnable {
public:
    void pushToQueue(T *element) {
        queue.push(element);
    }

protected:
    /**
     * Max size = -1 for unlimited.
     */
    explicit ThreadBuffer(int maxSize) : queue(maxSize) {
    }

    void run() override {
        while (!stopRequested()) {
            T *element = queue.pop();
            if (element != nullptr) {
                process(element);
                delete element;
            }
        }

        // flush
        while (!queue.empty()) {
            T *element = queue.pop();
            if (element != nullptr) {
                process(element);
                delete element;
            }
        }

        closeImpl();
    }

    void stopImpl() override {
        queue.push(nullptr);
        queue.wakeUp();
    }

    virtual void process(T *element) = 0;

    /**
     * Called when the consumer thread finished.
     */
    virtual void closeImpl() {
    }

private:
    BlockingQueue<T *> queue;
};


#endif //CONSUMER_H
