#include "Logger.h"
#include <ctime>
#include <iomanip>
#include <iostream>
#include "Utils.h"
#include <filesystem>

using namespace std;

static const string FILE_NAME = "log.txt";
static const long MAX_LOG_FILE_SIZE = 1000000; //1 MB

Logger::Logger() : ThreadBuffer(-1), threshold(Logger::Level::INFO), printDate(true) {
    try {
        filesystem::path path = filesystem::current_path() / FILE_NAME;
        if (filesystem::exists(path) && filesystem::file_size(path) > MAX_LOG_FILE_SIZE) {
            // remove the old copy and replace it with the current log file
            filesystem::path pathOld = filesystem::current_path() / (FILE_NAME + ".old");
            filesystem::remove(pathOld);
            filesystem::rename(path, pathOld);
        }

        fileOut.open(path, ofstream::app);
        fileOut << "==============================" << endl;
    } catch (exception &e) {
        cerr << "Could not open log file: " << e.what() << endl;
    }
}

Logger::~Logger() {
    fileOut.close();
}

Logger::Level Logger::getLevel() const {
    return threshold;
}

void Logger::setLevel(Level level) {
    threshold = level;
}

bool Logger::isPrintingDate() const {
    return printDate;
}

void Logger::setPrintDate(bool print) {
    printDate = print;
}

void Logger::logImpl(Level level, const string &message) {
    if (level >= threshold) {
        time_t t = time(nullptr);
        tm tm = *localtime(&t);

        ostringstream out;
        out << '[';
        if (printDate)
            out << put_time(&tm, "%d.%m.%Y %H:%M:%S") << ", ";
        out << LEVEL_NAMES[(unsigned int) level] << "] " << message;
        pushToQueue(new string(out.str()));
    }
}

void Logger::process(string *s) {
    if (fileOut.is_open())
        fileOut << *s << endl;
    cout << *s << endl;
}

void Logger::log(Logger::Level level, const string &message) {
    getInstance().logImpl(level, message);
}

void Logger::debug(const string &message) {
    getInstance().logImpl(Level::DEBUG, message);
}

void Logger::info(const string &message) {
    getInstance().logImpl(Level::INFO, message);
}

void Logger::warning(const string &message) {
    getInstance().logImpl(Level::WARNING, message);
}

void Logger::error(const string &message) {
    getInstance().logImpl(Level::ERROR, message);
}

void Logger::fatal(const string &message) {
    getInstance().logImpl(Level::FATAL, message);
}