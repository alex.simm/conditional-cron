#include "Utils.h"
#include <array>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <iterator>
#include <iomanip>

using namespace std;
using namespace std::chrono;
using namespace std::filesystem;

string Utils::append(const vector<string> &vec, const string &delimiter) {
    ostringstream out;
    copy(vec.begin(), vec.end(), ostream_iterator<string>(out, delimiter.c_str()));
    return out.str();
}

string Utils::trim(string s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !isspace(ch);
    }));
    s.erase(find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !isspace(ch);
    }).base(), s.end());
    return s;
}

vector<string> Utils::split(const string &s, const string &delimiter) {
    vector<string> v;

    size_t last = 0;
    size_t next;
    while ((next = s.find(delimiter, last)) != string::npos) {
        v.push_back(s.substr(last, next - last));
        last = next + 1;
    }
    v.push_back(s.substr(last));

    return v;
}

bool Utils::contains(const string &text, const string &query) {
    return text.find(query) != string::npos;
}

bool Utils::startsWith(const string &text, char c) {
    return !text.empty() && text.at(0) == c;
}

bool Utils::endsWith(const string &text, char c) {
    return !text.empty() && text.at(text.size() - 1) == c;
}

bool Utils::isCommandAvailable(const string &command) {
    string cmd = "which \"" + command + "\" > /dev/null 2>&1";
    return system(cmd.c_str()) == 0;
}

string Utils::runAndReadOutput(const string &command) {
    auto pipe = popen(command.c_str(), "r");
    if (pipe == nullptr) {
        throw runtime_error("Cannot open pipe");
    }

    array<char, 256> buffer;
    string result;
    while (not feof(pipe)) {
        auto bytes = fread(buffer.data(), 1, buffer.size(), pipe);
        result.append(buffer.data(), bytes);
    }

    pclose(pipe);
    return result;
}

map<string, string> Utils::readKeyValueFile(const string &fileName) {
    const char stripSymbols[] = {'"', '\''};

    ifstream input;
    input.exceptions(input.exceptions() | ios::failbit);

    map<string, string> map;
    try {
        input.open(fileName);

        string line, key, value;
        while (input.good() && getline(input, line)) {
            istringstream lineStream(line);
            if (getline(lineStream, key, '=') && getline(lineStream, value)) {
                // remove quotation marks from key and value
                for (char c : stripSymbols) {
                    if (key.size() > 2 && Utils::startsWith(key, c) && Utils::endsWith(key, c)) {
                        key = key.substr(1, key.size() - 2);
                    }
                    if (value.size() > 2 && Utils::startsWith(value, c) && Utils::endsWith(value, c)) {
                        value = value.substr(1, value.size() - 2);
                    }
                }

                map[key] = value;
            }
        }
    }
    catch (const ifstream::failure &e) {
        if (!input.eof())
            throw runtime_error("Could not find file: " + fileName);
    }

    input.close();

    return map;
}

void Utils::writeKeyValueFile(const string &filename, const map<string, string> &map) {
    ofstream out;
    out.open(filename);

    for (const auto &pair : map) {
        out << pair.first << "=" << pair.second << endl;
    }

    out.close();
}

chrono::system_clock::time_point Utils::parseDate(const string &date, const string &format) {
    tm tm = {};
    stringstream ss;
    ss.str(date);
    ss >> get_time(&tm, format.c_str());
    return system_clock::from_time_t(mktime(&tm));
}

string Utils::formatDate(chrono::system_clock::time_point date, const string &format) {
    std::time_t dateT = std::chrono::system_clock::to_time_t(date);
    std::stringstream str;
    str << std::put_time(localtime(&dateT), format.c_str());
    return str.str();
    /*string s(30, '\0');
    time_t time = system_clock::to_time_t(date);
    strftime(&s[0], s.size(), format.c_str(), localtime(&time));
    return s.substr();*/
}

string Utils::readFile(const string &fileName) {
    ifstream input;
    input.exceptions(input.exceptions() | ios::failbit);

    string data;
    try {
        input.open(fileName);
        if (!input.good())
            throw runtime_error("file cannot be opened: " + fileName);

        input.seekg(0, ios::end);
        data.reserve(input.tellg());
        input.seekg(0, ios::beg);
        data.assign((istreambuf_iterator<char>(input)), istreambuf_iterator<char>());
    }
    catch (const ifstream::failure &e) {
        if (!input.eof()) {
            try {
                input.close();
            } catch (exception &e) {
            }

            throw runtime_error("file cannot be read: " + fileName);
        }
    }

    try {
        input.close();
    } catch (exception &e) {
    }

    return data;
}

std::filesystem::path Utils::createTempFile() {
    string base = "condcron_";
    path dir = temp_directory_path();
    path p;
    do {
        auto timestamp = chrono::high_resolution_clock::now().time_since_epoch();
        p = dir / (base + to_string(std::chrono::duration_cast<std::chrono::milliseconds>(timestamp).count()));
    } while (exists(p));

    return p;
}