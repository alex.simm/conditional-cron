#include "VersionNumber.h"
#include "Utils.h"
#include <sstream>

using namespace std;

VersionNumber::VersionNumber(const std::vector<uint> &version) : version(version) {
}

VersionNumber::VersionNumber(const std::string &s) {
    vector<string> v = Utils::split(s, ".");
    version = Utils::applyTo<uint, string>(v, [](const string &x) {
        return atoi(x.c_str());
    });
}

bool VersionNumber::isValid() const {
    return !version.empty();
}

const std::vector<uint> &VersionNumber::getVersion() const {
    return version;
}

bool VersionNumber::operator==(const VersionNumber &v) {
    return v.version == version;
}

bool VersionNumber::operator>(const VersionNumber &v) {
    const int len = min(v.version.size(), version.size());

    // iterate until two entries are not equal
    int idx = 0;
    while (idx < len && version[idx] == v.version[idx]) {
        idx++;
    }

    if (idx == len) {
        if (version.size() > v.version.size())
            return true;
        else
            return false;
    } else
        return version[idx] > v.version[idx];
}

bool VersionNumber::operator<(const VersionNumber &v) {
    const int len = min(v.version.size(), version.size());

    // iterate until two entries are not equal
    int idx = 0;
    while (idx < len && version[idx] == v.version[idx]) {
        idx++;
    }

    if (idx == len) {
        if (version.size() < v.version.size())
            return true;
        else
            return false;
    } else
        return version[idx] < v.version[idx];
}

std::string VersionNumber::toString() const {
    ostringstream out;
    for (uint i = 0; i < version.size(); i++) {
        if (i > 0) out << '.';
        out << version[i];
    }
    return out.str();
}