#ifndef VERSIONNUMBER_H
#define VERSIONNUMBER_H

#include <string>
#include <vector>

class VersionNumber {
public:
    explicit VersionNumber(const std::vector<uint> &version = {});

    explicit VersionNumber(const std::string &s);

    bool isValid() const;

    const std::vector<uint> &getVersion() const;

    bool operator==(const VersionNumber &v);

    bool operator>(const VersionNumber &v);

    bool operator<(const VersionNumber &v);

    std::string toString() const;

private:
    std::vector<uint> version;
};

#endif //VERSIONNUMBER_H
