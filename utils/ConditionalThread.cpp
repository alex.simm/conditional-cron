#include "ConditionalThread.h"

using namespace std;
using namespace std::chrono;

bool ConditionalThread::sleepFor(const system_clock::duration &time) {
    sleeping = true;
    unique_lock<std::mutex> lock(mutex);
    return sleepCondition.wait_for(lock, time, [&] { return !sleeping || stopRequested(); });
}

void ConditionalThread::wakeUp() {
    std::unique_lock<std::mutex> lock(mutex);
    sleeping = false;
    sleepCondition.notify_all();
}

bool ConditionalThread::isSleeping() const {
    return sleeping;
}

void ConditionalThread::stopImpl() {
    wakeUp();
}
