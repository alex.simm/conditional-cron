#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <chrono>
#include <sstream>
#include <filesystem>

class Utils {
public:
    inline static const std::string DATE_FORMAT_STD = "%Y-%m-%d %H:%M:%S";

    static std::string append(const std::vector<std::string> &vec, const std::string &delimiter);

    /**
     * Applies a given lambda function to all elements of the vector and returns a new vector with the results.
     */
    template<class T1, class T2, typename F>
    static std::vector<T1> applyTo(const std::vector<T2> &src, F func) {
        std::vector<T1> dst;
        for (auto it = src.cbegin(); it != src.cend(); ++it) {
            dst.push_back(func(*it));
        }
        return dst;
    }

    /**
     * Trims whitespaces from a string on both ends.
     */
    static std::string trim(std::string s);

    template<class T>
    static std::string transform(const std::string &s, T function) {
        std::string copy = s;
        std::transform(copy.begin(), copy.end(), copy.begin(), function);
        return copy;
    }

    /**
     * Splits a string into parts based on a given delimiter.
     */
    static std::vector<std::string> split(const std::string &s, const std::string &delimiter);

    static bool contains(const std::string &text, const std::string &query);

    static bool startsWith(const std::string &text, char c);

    static bool endsWith(const std::string &text, char c);

    template<class K, class V>
    static bool contains(const std::map<K, V> &map, const K &key) {
        return map.find(key) != map.end();
    }

    /**
     * Tests if a command is available on this system.
     */
    static bool isCommandAvailable(const std::string &command);

    /**
     * Runs a command and returns the full, raw output.
     */
    static std::string runAndReadOutput(const std::string &command);

    /**
     * Reads and parses lines from a file as key-value-pairs separated by an equals sign.
     */
    static std::map<std::string, std::string> readKeyValueFile(const std::string &fileName);

    /**
     * Writes key-value-pairs as lines to a file using a specified separator.
     */
    static void writeKeyValueFile(const std::string &filename, const std::map<std::string, std::string> &map);

    static std::string formatDate(std::chrono::system_clock::time_point date,
                                  const std::string &format = DATE_FORMAT_STD);

    static std::chrono::system_clock::time_point parseDate(const std::string &date,
                                                           const std::string &format = DATE_FORMAT_STD);

    /**
     * Reads a whole file into memory.
     */
    static std::string readFile(const std::string &fileName);

    template<class T>
    static std::string toString(const std::vector<T> &vec, const std::string &delimiter = ",",
                                const std::string quotation = "") {
        std::ostringstream str;
        for (typename std::vector<T>::const_iterator iter = vec.begin(); iter != vec.end(); ++iter) {
            if (iter != vec.begin())
                str << delimiter;
            str << quotation << *iter << quotation;
        }
        return str.str();
    }

    static std::filesystem::path createTempFile();

    template<class K, class V>
    static std::vector<V> values(const std::map<K, V> &map) {
        std::vector<V> dst;
        for (auto const &imap: map) {
            dst.push_back(imap.second);
        }
        return dst;
    }

    template<class T, typename F>
    static std::vector<T> filter(const std::vector<T> v, F condition) {
        std::vector<T> dst;
        for(T t : v) {
            if (condition(t))
                dst.push_back(t);
        }
        return dst;
    }
};

#endif //UTILS_H
