#ifndef RUNNABLE_H
#define RUNNABLE_H

#include <thread>
#include <iostream>
#include <future>

/**
 * A thread that can be started and stopped.
 */
class Runnable {
public:
    Runnable() : exitSignal(), futureObject(exitSignal.get_future()) {
    }

    Runnable(Runnable &obj) : exitSignal(std::move(obj.exitSignal)), futureObject(std::move(obj.futureObject)) {
        operator=(obj);
    }

    Runnable &operator=(Runnable &obj) {
        if (&obj != this) {
            exitSignal = std::move(obj.exitSignal);
            futureObject = std::move(obj.futureObject);
            thread = obj.thread;
        }
        return *this;
    }

    virtual ~Runnable() {
        stop();
    }

    void start() {
        if (thread == nullptr) {
            thread = new std::thread(staticRun, this);
        }
    }

    void stop() {
        if (!stopRequested() && thread != nullptr) {
            exitSignal.set_value();
            stopImpl();
            thread->join();
            delete thread;
            thread = nullptr;
        }
    }

    void join() const {
        if (!stopRequested() && thread != nullptr && thread->joinable())
            thread->join();
    }

    bool isRunning() const {
        return thread != nullptr;
    }

protected:
    bool stopRequested() const {
        return futureObject.wait_for(std::chrono::milliseconds(0)) != std::future_status::timeout;
    }

    /**
     * Called after sending the stop signal and before joining the thread. Can be overridden by implementations.
     */
    virtual void stopImpl() {
    }

    virtual void run() = 0;

private:
    std::promise<void> exitSignal;
    std::future<void> futureObject;
    std::thread *thread = nullptr;

    static void staticRun(Runnable *r) {
        r->run();
        // r->exitSignal.set_value();
        r->thread = nullptr;
    }
};


#endif //RUNNABLE_H
