#ifndef CONDITIONALTHREAD_H
#define CONDITIONALTHREAD_H

#include "Runnable.h"
#include <mutex>
#include <condition_variable>
#include <chrono>

/**
 * A thread that can sleep for a fixed time and be woken up from the outside.
 */
class ConditionalThread : public Runnable {
public:
    /**
     * Sleeps for the specified duration or until this thread is woken up. Returns whether it was woken up.
     */
    bool sleepFor(const std::chrono::system_clock::duration &time);

    /**
     * Wakes up this thread, if it is sleeping.
     */
    void wakeUp();

    /**
     * Returns whether this thread is currently sleeping.
     */
    bool isSleeping() const;

protected:
    void stopImpl() override;

private:
    mutable std::condition_variable sleepCondition;
    mutable std::mutex mutex;
    bool sleeping = false;
};


#endif //CONDITIONALTHREAD_H
