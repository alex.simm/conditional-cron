#include "ConfigParser.h"
#include <stdexcept>
#include "utils/Logger.h"
#include "api/notifications/LinuxDesktopNotification.h"
#include "api/tasks/CommandTask.h"
#include "api/tasks/PackageUpdateTask.h"
#include "api/tasks/SystemUpgradeTask.h"
#include "api/tasks/TestTask.h"
#include "api/conditions/HostReachable.h"
#include "utils/Utils.h"
#include "api/authentication/GnomeKeyring.h"
#include "api/tasks/DownloadTask.h"
#include "gui/TrayIcon.h"

using namespace std;
using namespace std::chrono;
using namespace nlohmann;

using days = duration<int64_t, ratio<24 * 3600>>;
using weeks = duration<int64_t, ratio<7 * 24 * 3600>>;
using months = duration<int64_t, ratio<30 * 24 * 3600>>;
using years = duration<int64_t, ratio<365 * 24 * 3600>>;

NotificationProvider *ConfigParser::parseNotificationProvider(const json &config) {
    Logger::debug("ConfigParser: parsing notification providers...");
    if (!config.is_object() || !config.contains("type"))
        return nullptr;

    string type = config["type"];
    if (type == "linux-desktop") {
        return new LinuxDesktopNotification();
    } else {
        Logger::warning("ConfigParser: unknown notification type: " + type);
        return nullptr;
    }
}

AuthenticationProvider *ConfigParser::parseAuthenticationProvider(const json &config) {
    Logger::debug("ConfigParser: parsing authentification providers...");
    if (!config.is_object() || !config.contains( "type"))
        return nullptr;

    string type = config["type"];
    if (type == "gnome-keyring") {
        return new GnomeKeyring();
    } else {
        Logger::warning("ConfigParser: unknown authentication type: " + type);
        return nullptr;
    }
}

seconds ConfigParser::parseInterval(const json &config) {
    if (!config.is_object())
        throw runtime_error("ConfigParser: interval must be an object");

    seconds interval(0);
    for (auto it = config.begin(); it != config.end(); ++it) {
        string x = Utils::trim(it.key());
        if (x == "seconds") {
            interval += seconds(it.value());
        } else if (x == "minutes") {
            interval += minutes(it.value());
        } else if (x == "hours") {
            interval += hours(it.value());
        } else if (x == "days") {
            interval += days(it.value());
        } else if (x == "weeks") {
            interval += weeks(it.value());
        } else if (x == "months") {
            interval += months(it.value());
        } else if (x == "years") {
            interval += years(it.value());
        } else {
            Logger::warning("ConfigParser: invalid duration type: '" + it.key() + "'");
        }
    }

    if (interval.count() == 0)
        throw runtime_error("ConfigParser: interval must not be empty");

    return interval;
}

Condition *ConfigParser::parseCondition(const string &id, const json &config) {
    // check data
    if (!config.is_object())
        throw runtime_error("ConfigParser: condition must be an object");
    if (!config.contains( "type"))
        throw runtime_error("ConfigParser: condition must contain a 'type'");

    string type = config["type"];
    if (type == "host-reachable") {
        if (!config.contains( "host"))
            throw runtime_error("ConfigParser: condition 'host available' must contain a host");
        return new HostReachable(id, config["host"]);
    } else {
        Logger::warning("ConfigParser: unknown condition type: " + type);
        return nullptr;
    }
}

map<string, Condition *> ConfigParser::parseConditions(const json &config) {
    Logger::debug("ConfigParser: parsing conditions...");
    if (!config.is_object())
        throw runtime_error("ConfigParser: conditions must be an object");

    map<string, Condition *> map;

    for (auto it = config.begin(); it != config.end(); ++it) {
        Condition *c = ConfigParser::parseCondition(Utils::trim(it.key()), it.value());
        if (c != nullptr) {
            Logger::debug("ConfigParser: parsed condition: " + c->toString());
            map.insert(pair<string, Condition *>(it.key(), c));
        }
    }

    return map;
}

Task *ConfigParser::parseTask(const string &id, const json &config, const map<string, Condition *> &conditions) {
    // check data
    if (!config.is_object())
        throw runtime_error("ConfigParser: task must be an object");
    if (!config.contains( "type"))
        throw runtime_error("ConfigParser: task must contain a 'type' property");

    // parse type and create object
    string type = config["type"];
    Task *task;
    if (type == "command") {
        if (!config.contains( "commands"))
            throw runtime_error("ConfigParser: command task must contain a command");
        task = new CommandTask(id, config["commands"]);
    } else if (type == "package-update") {
        task = new PackageUpdateTask(id);
    } else if (type == "system-upgrade") {
        task = new SystemUpgradeTask(id);
    } else if (type == "test") {
        task = new TestTask(id, config["sleepSeconds"]);
    } else if (type == "download") {
        string user = config.contains( "user") ? config["user"] : "";
        string authKey = config.contains( "authkey") ? config["authkey"] : "";
        auto t = new DownloadTask(id, config["url"], config["destination"], user, authKey);
        if (config.contains( "compress"))
            t->setCompress(config["compress"]);
        if (config.contains( "http-options"))
            t->setHTTPOptions(config["http-options"]);
        task = t;
    } else {
        Logger::warning("ConfigParser: unknown task type: " + type);
        return nullptr;
    }

    if (task != nullptr) {
        // parse interval
        seconds onSuccess, onFailure;
        if (config.contains( "interval-on-success") && config.contains( "interval-on-failure")) {
            onSuccess = parseInterval(config["interval-on-success"]);
            onFailure = parseInterval(config["interval-on-failure"]);
        } else if (config.contains( "interval")) {
            onSuccess = parseInterval(config["interval"]);
            onFailure = onSuccess;
        } else {
            throw runtime_error(
                    "ConfigParser: task must either have a 'interval' or both 'interval-on-success' and 'interval-on-failure' properties");
        }

        if (onSuccess.count() == 0 || onFailure.count() == 0)
            throw runtime_error("ConfigParser: task intervals must not be 0");
        task->setUpdateInterval(onSuccess, true);
        task->setUpdateInterval(onFailure, false);

        // assign conditions to task
        if (config.contains( "conditions")) {
            json conditionIds = config["conditions"];
            if (!conditionIds.is_array())
                throw runtime_error("ConfigParser: task conditions must be an array");

            task->setConditions(findTaskConditions(conditions, conditionIds));
        }
    }

    return task;
}

vector<Task *> ConfigParser::parseTasks(const json &config, const map<string, Condition *> &conditions) {
    Logger::debug("ConfigParser: parsing tasks...");
    if (!config.is_object())
        throw runtime_error("ConfigParser: tasks must be an object");

    vector<Task *> tasks;

    for (auto it = config.begin(); it != config.end(); ++it) {
        Task *task = ConfigParser::parseTask(Utils::trim(it.key()), it.value(), conditions);
        if (task != nullptr) {
            Logger::debug("ConfigParser: parsed task '" + task->getID() + "': " + task->toString());
            tasks.push_back(task);
        }
    }

    return tasks;
}

std::vector<Condition *> ConfigParser::findTaskConditions(std::map<std::string, Condition *> conditions,
                                                          const nlohmann::json &conditionIds) {
    vector<Condition *> taskConditions = {};

    if (!conditionIds.is_array())
        throw runtime_error("ConfigParser: task conditions must be an array");

    for (const json &conditionId : conditionIds) {
        if (!conditionId.is_string())
            Logger::warning("ConfigParser: task conditions must be strings");

        try {
            taskConditions.push_back(conditions.at(conditionId));
        } catch (out_of_range &e) {
            Logger::warning("ConfigParser: no condition found for ID '"
                            + string(conditionId) + "'");
        }
    }

    return taskConditions;
}

void *ConfigParser::createGUI(const nlohmann::json &config, Scheduler &scheduler) {
    Logger::debug("ConfigParser: creating GUI...");
    if (!config.is_object() || !config.contains( "type"))
        return nullptr;

    string type = config["type"];
    if (type == "gtk-tray") {
        auto icon = new TrayIcon(scheduler);
        icon->start();
        return icon;
    } else {
        Logger::warning("ConfigParser: unknown GUI type: " + type);
        return nullptr;
    }
}