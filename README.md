# conditional-cron

Task scheduler for linux with conditions. Similar to cron, each task can have an update interval. Additionally, tasks 
can depend on multiple conditions which are updated regularly. If the conditions for a task are not fulfilled
its execution will be postponed.

Main components:
* Conditions: will be checked repeatedly
* Authentications: providers for passwords, e.g. local keychains
* Tasks: will be executed repeatedly and depend on conditions
* Notifications: provide a way to notify users about tasks, e.g. via desktop notifications



### Configuration
Configuration is done with a `config.json` file. Example:
```
{
  "initial_delay": {
    "seconds": 3
  },
  "notifications": {
    "linux-desktop": {}
  },
  "conditions": {
    "has-internet": {
      "type": "host-reachable",
      "host": "wikipedia.org"
    },
    "my-server-reachable": {
      "type": "host-reachable",
      "host": "192.168.178.1"
    }
  },
  "tasks": {
    "update-packages": {
      "type": "package-update",
      "interval": {
        "days": 1
      }
    },
    "system-upgrade": {
      "type": "system-upgrade",
      "interval": {
        "weeks": 1
      }
    },
    "file-sync": {
      "type": "command",
      "interval": {
        "hours": 2
        "minutes": 30
      },
      "commands": ["rclone -v sync /home/someone/files/ destination:/home/someone/backup"],
      "conditions": ["my-server-reachable"]
    },
    "do-something": {
      "type": "command",
      "interval": {
        "weeks": 3
      },
      "commands": [
        "echo 'Hello world!'",
        "ls -la"
      ]
    }
  }
}
```

* Intervals are objects with one or more entry with name `seconds`, `minutes`, `hours`, `days`, `weeks`, `months`, or `years`. Multiple entries per object will be summed up, e.g.`{"weeks": 1, "days": 2, "hours": 3}` corresponds to 9 days and 3 hours.
* `initial delay`: overall start time before the main task scheduler begins.
* The ID of tasks and conditions is given by the name of the JSON object.
* Conditions are defined in a global list and referenced from the tasks by their ID. For example, in the configuration above the task `file-sync` depends on the condition `my-server-reachable`.
* `notifications` lists the implementations of the `Notification` interface that will be used for notifying the user about tasks and conditions. (see `api/Notification.h`)



### Implemented tasks

##### CommandTask

Accepts any list of commands and runs them. Arguments:

* `commands`(array,required): a list of commands that will be executed consecutively

##### PackageUpdateTask

Updates system packages using apt or dnf. (no arguments)

##### SystemUpgradeTask

Notifies the user if a major release of the operating system is available. (no arguments)

##### DownloadTask

Downloads a file from HTTP, optionally compresses it with bzip2, and stores it in a local file. Arguments:

* `url`(string,required) the URL from which to download
* `destination`(string,required) path of a local file. An existing file will be overwritten.
* `compress`(boolean) if the downloaded file should be compressed with bzip2
* `user`(string) user name for HTTP-authentication
* `authkey`(string) key under which the password for HTTP-authentication is stored in the keychain (see 'Authentications')
* `http-options`(object):
  * `method`(string) HTTP request method

##### TestTask

Only contains a delay by a specified of seconds, for testing purposes. Arguments:

* `sleepSeconds`(integer,required) number of seconds to sleep



### Implemented conditions

* `HostReachable` uses an ICMP ping to determine if a host is reachable

