#include <stdexcept>
#include "TimestampRegistry.h"
#include "utils/Utils.h"
#include "utils/Logger.h"

using namespace std;
using namespace std::chrono;
using namespace sqlite3x;

static const string TIMESTAMP_TABLE = "task_last_execution";
static const string FILENAME = "./conditional-cron.db";

TimestampRegistry::TimestampRegistry() : connection(FILENAME), fallbackTime(system_clock::time_point()) {
    // test if table exists
    bool tableExists = false;
    try {
        sqlite3_command command(connection,
                                "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='"
                                + TIMESTAMP_TABLE + "'");
        int count = command.executeint();
        tableExists = count > 0;
    } catch (exception &e) {
    }

    // create the table, if necessary
    if (!tableExists) {
        try {
            sqlite3_command command(connection,
                                    "CREATE TABLE " + TIMESTAMP_TABLE + " ("
                                    + "task_id VARCHAR(255) NOT NULL,"
                                    + "timestamp TIMESTAMP NOT NULL,"
                                    + "PRIMARY KEY (task_id)"
                                    + ")");
            command.executenonquery();
        } catch (exception &e) {
            Logger::debug("Database: error when creating database table: " + string(e.what()));
        }
    }
}

std::chrono::system_clock::time_point TimestampRegistry::getLastExecution(Task *task) {
    try {
        sqlite3_command command(connection, "SELECT timestamp FROM " + TIMESTAMP_TABLE + " WHERE task_id=?");
        command.bind(1, task->getID());
        string timestamp = command.executestring();
        return Utils::parseDate(timestamp, Utils::DATE_FORMAT_STD);
    } catch (exception &e) {
        Logger::warning("Database: no timestamp found for task " + task->getID() + ", " + string(e.what()));
        setLastExecution(task, fallbackTime);
        return fallbackTime;
    }
}

void TimestampRegistry::setLastExecution(Task *task, std::chrono::system_clock::time_point time) {
    try {
        sqlite3_command command(connection, "INSERT INTO " + TIMESTAMP_TABLE + " (task_id,timestamp) VALUES (?,?)");
        command.bind(1, task->getID());
        command.bind(2, Utils::formatDate(time, Utils::DATE_FORMAT_STD));
        command.executenonquery();
    } catch (exception &e) {
        // if insert fails, the row might already be there -> try update
        sqlite3_command command(connection, "UPDATE " + TIMESTAMP_TABLE + " SET timestamp=? WHERE task_id=?");
        command.bind(1, Utils::formatDate(time, Utils::DATE_FORMAT_STD));
        command.bind(2, task->getID());
        command.executenonquery();
    }
}
