#ifndef TRAYICON_H
#define TRAYICON_H

#include <map>
#include <libappindicator/app-indicator.h>
#include "../utils/Runnable.h"
#include "../Scheduler.h"

class TrayIcon : public Runnable, public SchedulerListener {
public:
    explicit TrayIcon(Scheduler &scheduler);

    void run() override;

    void statusChanged(Status status, Task *task) override;

private:
    Scheduler &scheduler;
    AppIndicator *indicator;
    GtkMenuShell *menu;
    GtkWidget *quitItem;
    std::map<GtkWidget *, Task *> taskItems;

    void onMenuItemClick(GtkWidget *item);

    GtkMenuShell *createMenu(GtkMenuShell *parent, std::string name, bool enabled = true);

    GtkWidget *createMenuItem(GtkMenuShell *parent, std::string name, bool enabled = true, bool addCallback = true);

    static void menuItemCallback(GtkWidget *item, gpointer data) {
        auto t = static_cast<TrayIcon *>(data);
        t->onMenuItemClick(item);
    }
};

#endif //TRAYICON_H
