#include "TrayIcon.h"
#include <stdexcept>

using namespace std;

static const string ICON_IDLE = "cs-xlet-running";
static const string ICON_RUNNING = "cs-xlet-update";
static const string ICON_STOPPED = "cs-xlet-error";

TrayIcon::TrayIcon(Scheduler &scheduler) : scheduler(scheduler) {
    if (gtk_init_check(nullptr, nullptr) == FALSE)
        throw runtime_error("TrayIcon: could not initialise");

    // create menu
    menu = (GtkMenuShell *) gtk_menu_new();
    GtkMenuShell *taskMenu = createMenu(menu, "Tasks");
    for (Task *task : scheduler.getTasks()) {
        GtkWidget *item = createMenuItem(taskMenu, task->getID());
        taskItems.insert({item, task});
    }

    quitItem = createMenuItem(menu, "Quit");

    // create tray icon
    indicator = app_indicator_new("cond-cron-tray", ICON_IDLE.c_str(), APP_INDICATOR_CATEGORY_APPLICATION_STATUS);
    app_indicator_set_status(indicator, APP_INDICATOR_STATUS_ACTIVE);
    app_indicator_set_menu(indicator, GTK_MENU(menu));

    statusChanged(scheduler.getStatus(), nullptr);
    scheduler.addListener(this);
}

void TrayIcon::run() {
    do {
        gtk_main_iteration_do(1);
    } while (!stopRequested());
}

GtkMenuShell *TrayIcon::createMenu(GtkMenuShell *parent, std::string name, bool enabled) {
    GtkWidget *item = createMenuItem(parent, name, enabled, false);
    auto m = (GtkMenuShell *) gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), GTK_WIDGET(m));

    return m;
}

GtkWidget *TrayIcon::createMenuItem(GtkMenuShell *parent, std::string name, bool enabled, bool addCallback) {
    GtkWidget *item = gtk_menu_item_new_with_label(name.c_str());
    gtk_widget_set_sensitive(item, enabled);
    if (addCallback)
        g_signal_connect(item, "activate", G_CALLBACK(menuItemCallback), this);

    gtk_widget_show(item);
    gtk_menu_shell_append(parent, item);
    return item;
}

void TrayIcon::onMenuItemClick(GtkWidget *item) {
    if (item == quitItem) {
        scheduler.stop();
        stop();
    } else {
        if (Utils::contains(taskItems, item)) {
            Task *task = taskItems[item];
            if (task)
                scheduler.forceRun(task);
        }
    }
}

void TrayIcon::statusChanged(Status status, Task *task) {
    string icon, label;
    switch (status) {
        case Status::STOPPED:
            label = "stopped";
            icon = ICON_STOPPED;
            break;
        case Status::RUNNING_TASK:
            label = "running: " + task->getID();
            icon = ICON_RUNNING;
            break;
        default:
            label = "idle";
            icon = ICON_IDLE;
            break;
    }

    app_indicator_set_title(indicator, label.c_str());
    app_indicator_set_icon(indicator, icon.c_str());
}
