#include "Scheduler.h"
#include <algorithm>
#include "utils/Utils.h"
#include "utils/Logger.h"

using namespace std;
using namespace std::chrono;

Scheduler::Scheduler(const vector<Task *> &tasks, const vector<Condition *> &conditions,
                     const ProviderStore &providers)
        : tasks(tasks), conditions(conditions), providers(providers) {
    // set next-execution timestamps for tasks
    for (Task *t : tasks) {
        time_point timestamp = lastTaskExecution.getLastExecution(t) + t->getUpdateInterval(true);
        nextTaskExecution.insert({t, timestamp});

        Logger::info("Scheduler: task " + t->getID() + ": scheduled=" + Utils::formatDate(timestamp) + ";");
    }

    // set all conditions to be updated
    for (const auto &c : conditions) {
        nextConditionUpdate[c] = system_clock::now();
    }
}

const std::vector<Task *> &Scheduler::getTasks() const {
    return tasks;
}

SchedulerListener::Status Scheduler::getStatus() const {
    return currentStatus;
}

void Scheduler::forceRun(Task *task) {
    if (!stopRequested()) {
        forcedTask = task;
        wakeUp();
    }
}

void Scheduler::updateState(SchedulerListener::Status status, Task *task) {
    currentStatus = status;
    for (SchedulerListener *l : getListeners()) {
        l->statusChanged(status, task);
    }
}

void Scheduler::stopImpl() {
    ConditionalThread::stopImpl();
    updateState(SchedulerListener::Status::STOPPED, nullptr);
}

void Scheduler::run() {
    if (tasks.empty()) {
        updateState(SchedulerListener::Status::STOPPED, nullptr);
        return;
    } else {
        updateState(SchedulerListener::Status::IDLE, nullptr);
    }

    // endless loop for scheduler
    while (!stopRequested()) {
        updateConditions(conditions);

        if (forcedTask) {
            updateConditions(forcedTask->getConditions(), true);

            runTask(forcedTask, true);
            forcedTask = nullptr;
        } else {
            updateTasks();
        }

        // sleep until the next task or condition update is due
        auto nextLoopTime = findNextLoopTime();
        system_clock::duration sleepDuration;
        if (nextLoopTime > system_clock::now()) {
            Logger::info("Scheduler: next loop time: " + Utils::formatDate(nextLoopTime));
            sleepDuration = nextLoopTime - system_clock::now();
        } else {
            sleepDuration = seconds(1);
        }
        Logger::info("Scheduler: idle for " + to_string(duration_cast<seconds>(sleepDuration).count()) + " seconds");
        sleepFor(sleepDuration);
    }

    updateState(SchedulerListener::Status::STOPPED, nullptr);
}

void Scheduler::updateConditions(const vector<Condition *> &conditionList, bool force) {
    for (auto it = conditionList.begin(); it != conditionList.end() && !stopRequested(); ++it) {
        Condition *condition = *it;
        if (force || nextConditionUpdate[condition] < system_clock::now()) {
            condition->update();
            nextConditionUpdate[condition] = system_clock::now()
                                             + condition->getUpdateInterval(condition->isFulfilled());
            Logger::info("Scheduler: updated condition '" + condition->getID()
                         + "', next scheduled update: " + Utils::formatDate(nextConditionUpdate[condition]));
        }
    }
}

void Scheduler::updateTasks() {
    for (auto it = tasks.begin(); it != tasks.end() && !stopRequested(); ++it) {
        Task *task = *it;

        if (nextTaskExecution[task] <= system_clock::now())
            runTask(task, false);
    }
}

void Scheduler::runTask(Task *task, bool showNotifications) {
    // check if all conditions are fulfilled
    Logger::info("Scheduler: checking task " + task->getID());
    const std::vector<Condition *> &taskConditions = task->getConditions();
    auto notFulfilled = Utils::filter(taskConditions, [](Condition *c) { return !c->isFulfilled(); });
    Logger::debug("Scheduler: task=" + task->getID() + ", conditions=" + to_string(taskConditions.size())
                  + ", fulfilled=" +
                  to_string(taskConditions.size() - std::distance(begin(notFulfilled), end(notFulfilled))));

    if (notFulfilled.empty()) {
        // run if all conditions are fulfilled
        Logger::info("Scheduler: starting task '" + task->getID() + "'");
        if (showNotifications && providers.getNotificationProvider())
            providers.getNotificationProvider()->notify(NotificationProvider::NotificationType::INFO,
                                                        forcedTask->getID(), "Starting task " + forcedTask->getID());

        try {
            updateState(SchedulerListener::Status::RUNNING_TASK, task);
            task->run(providers);

            Logger::info("Scheduler: finished task '" + task->getID()
                         + "', next scheduled execution: " + Utils::formatDate(nextTaskExecution[task]));
            if (showNotifications && providers.getNotificationProvider())
                providers.getNotificationProvider()->notify(NotificationProvider::NotificationType::INFO,
                                                            forcedTask->getID(), "Task finished successfully");

            // update timestamps and schedule next execution
            lastTaskExecution.setLastExecution(task, system_clock::now());
            nextTaskExecution[task] = system_clock::now() + task->getUpdateInterval(true);
        } catch (exception &e) {
            Logger::error("Scheduler: task '" + task->getID() + "' threw error: " + string(e.what()));
            if (showNotifications && providers.getNotificationProvider())
                providers.getNotificationProvider()->notify(NotificationProvider::NotificationType::ERROR,
                                                            forcedTask->getID(),
                                                            "Task threw an error: " + string(e.what()));

            nextTaskExecution[task] += task->getUpdateInterval(false);
        }
    } else {
        // if not all conditions are fulfilled, set the next update time to the time at which a condition will be checked
        if (showNotifications && providers.getNotificationProvider())
            providers.getNotificationProvider()->notify(NotificationProvider::NotificationType::ERROR,
                                                        forcedTask->getID(), "Not all conditions are fulfilled");

        auto next = std::min_element(notFulfilled.begin(), notFulfilled.end(), [this](Condition *c1, Condition *c2) {
            return nextConditionUpdate[c1] < nextConditionUpdate[c2];
        });

        nextTaskExecution[task] = nextConditionUpdate[*next] + (*next)->getUpdateInterval(false);
    }

    updateState(SchedulerListener::Status::IDLE, nullptr);
}

std::chrono::system_clock::time_point Scheduler::findNextLoopTime() {
    auto nextCondition = std::min_element(conditions.begin(), conditions.end(),
                                             [this](Condition *c1, Condition *c2) {
                                                 return nextConditionUpdate[c1] < nextConditionUpdate[c2];
                                             });

    auto nextTask = std::min_element(tasks.begin(), tasks.end(), [this](Task *t1, Task *t2) {
        return nextTaskExecution[t1] < nextTaskExecution[t2];
    });

    return min(nextConditionUpdate[*nextCondition], nextTaskExecution[*nextTask]);
}
