#include "Condition.h"

using namespace std;
using namespace std::chrono;

Condition::Condition(const std::string &id, std::chrono::seconds updateIntervalFulfilled,
                     std::chrono::seconds updateIntervalNotFulfilled)
        : Entity(id), fulfilled(false),
          updateIntervalFulfilled(updateIntervalFulfilled),
          updateIntervalNotFulfilled(updateIntervalNotFulfilled) {
}

bool Condition::isFulfilled() const {
    return fulfilled;
}

void Condition::update() {
    fulfilled = updateImpl();
}

std::chrono::seconds Condition::getUpdateInterval(bool ifFulfilled) const {
    return ifFulfilled ? updateIntervalFulfilled : updateIntervalNotFulfilled;
}

void Condition::setUpdateInterval(std::chrono::seconds interval, bool ifFulfilled) {
    if (ifFulfilled)
        updateIntervalFulfilled = interval;
    else
        updateIntervalNotFulfilled = interval;
}
