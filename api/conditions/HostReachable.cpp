#include <stdexcept>
#include "HostReachable.h"
//#include <pcapplusplus/NetworkUtils.h>
//#include <pcapplusplus/PcapLiveDeviceList.h>

using namespace std;
using namespace chrono;

HostReachable::HostReachable(const string &id, const string &host)
        : Condition(id, duration_cast<seconds>(minutes(30)), minutes(5)),
          host(host) {
    if (host.empty())
        throw runtime_error("HostReachable: host must not be empty");
}

string HostReachable::toString() {
    return "host available: host=" + host;
}

bool HostReachable::updateImpl() {
    /*
    // get list of non-loopback devices
    const vector<PcapLiveDevice *> &devices = PcapLiveDeviceList::getInstance().getPcapLiveDevicesList();
    auto filtered = devices | views::filter([](PcapLiveDevice *d) { return !d->getLoopback(); });

    for (PcapLiveDevice *device : filtered) {
        // try to find the IP address first, if none given
        Logger::debug("testing device: " + string(device->getName()));
        bool result = pingOnDevice(device);
        Logger::debug("====================================");
        if (device->isOpened()) {
            try {
                device->close();
            } catch (exception &e) {
            }
        }
        if (result)
            return true;
    }

    return false;*/

    string command = "ping -c 1 " + host;
    return system(command.c_str()) == 0;
}

/*bool HostAvailable::pingOnDevice(pcpp::PcapLiveDevice *device) {
    try {
        if (device->getLoopback())
            return false;

        if (!device->open())
            return false;

        MacAddress sourceMac = device->getMacAddress();
        if (!sourceMac.isValid() || sourceMac == MacAddress::Zero)
            return false;
        Logger::debug("sourceMac: " + sourceMac.toString());
        IPv4Address sourceIP = device->getIPv4Address();
        if (!sourceIP.isValid() || sourceIP == IPv4Address::Zero)
            return false;
        Logger::debug("sourceIP: " + sourceIP.toString());

        // try to resolve the hostname first
        Logger::debug("ip before: " + ipAddress.toString());
        if (ipAddress == IPv4Address::Zero) {
            double responseTime;
            uint32_t ttl;
            ipAddress = NetworkUtils::getInstance().getIPv4Address(host, device, responseTime, ttl);
        }
        Logger::debug("ip after: " + ipAddress.toString());
        if (ipAddress == IPv4Address::Zero)
            return false;

        // find the MAC address for the given IP address
        double responseTime;
        MacAddress result = NetworkUtils::getInstance().getMacAddress(ipAddress, device, responseTime, sourceMac,
                                                                      sourceIP, NetworkUtils::DefaultTimeout);
        Logger::debug("mac result: " + result.toString());
        return result != MacAddress::Zero;
    }
    catch (exception &e) {
        Logger::error(e.what());
        return false;
    }
}*/
