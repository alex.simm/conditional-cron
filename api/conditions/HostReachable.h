#ifndef HOSTAVAILABLE_H
#define HOSTAVAILABLE_H

#include "../Condition.h"
#include <string>

/**
 * Checks if a host (given by hostname or IP address) is reachable by pinging it.
 */
class HostReachable : public Condition {
public:
    HostReachable(const std::string &id, const std::string &host);

    std::string toString() override;

private:
    std::string host;

    bool updateImpl() override;

    /**
     * Pings the host on the given device and returns if it was successful.
     */
    //bool pingOnDevice(pcpp::PcapLiveDevice *device);
};

#endif //HOSTAVAILABLE_H
