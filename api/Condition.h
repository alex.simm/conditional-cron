#ifndef CONDITION_H
#define CONDITION_H

#include <string>
#include <vector>
#include <chrono>
#include "Entity.h"

/**
 * Represents a condition on which tasks can depend. Conditions need to be checked repeatedly.
 */
class Condition : public Entity {
public:
    /**
     * Creates a passive condition with specific intervals in which the status should be updated.
     * @param updateIntervalFulfilled update interval while the condition is fulfilled
     * @param updateIntervalNotFulfilled update interval while the condition is not fulfilled
     */
    Condition(const std::string &id, std::chrono::seconds updateIntervalFulfilled,
              std::chrono::seconds updateIntervalNotFulfilled);

    virtual ~Condition() = default;

    /**
     * Returns a string representation of this condition.
     */
    virtual std::string toString() = 0;

    /**
     * Updates the condition's status by checking if it is fulfilled.
     */
    void update();

    /**
     * Returns whether the condition was fulfilled in the last update.
     */
    bool isFulfilled() const;

    /**
     * Returns the time interval after which this condition should be updated. Only valid for passive conditions.
     *
     * @param whether to return the interval for the fulfilled state
     */
    std::chrono::seconds getUpdateInterval(bool ifFulfilled) const;

    /**
     * Sets one of the two time intervals after which this condition should be updated.
     *
     * @param interval an interval
     * @param ifFulfilled whether this is the new interval for the fulfilled state
     */
    void setUpdateInterval(std::chrono::seconds interval, bool ifFulfilled);

protected:
    bool fulfilled;

    /**
     * Implements the actual update and returns whether it was successful.
     */
    virtual bool updateImpl() = 0;

private:
    std::chrono::seconds updateIntervalFulfilled, updateIntervalNotFulfilled;
};

#endif //CONDITION_H
