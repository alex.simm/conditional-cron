#include "Task.h"
#include <sstream>

using namespace std;
using namespace std::chrono;

Task::Task(const std::string &id)
        : Entity(id), updateIntervalSuccess(0), updateIntervalFailure(minutes(1)), conditions() {
}

std::string Task::toString() const {
    ostringstream os;
    os << getDescription() << ", repetition: on success=" << getUpdateInterval(true).count() << " minutes"
       << ", on failure=" << getUpdateInterval(false).count() << " minutes";
    for (Condition *c: conditions) {
        os << endl << "\t" << c->toString();
    }
    return os.str();
}

const std::vector<Condition *> &Task::getConditions() const {
    return conditions;
}

void Task::setConditions(const std::vector<Condition *> &cond) {
    conditions = cond;
}

seconds Task::getUpdateInterval(bool success) const {
    return success ? updateIntervalSuccess : updateIntervalFailure;
}

void Task::setUpdateInterval(std::chrono::seconds interval, bool success) {
    if (success)
        updateIntervalSuccess = interval;
    else
        updateIntervalFailure = interval;
}