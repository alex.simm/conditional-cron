#include "ProviderStore.h"

ProviderStore::ProviderStore(const ProviderStore &p) {
    this->operator=(p);
}

ProviderStore &ProviderStore::operator=(const ProviderStore &p) {
    if (this != &p) {
        authenticationProvider = p.authenticationProvider;
        notificationProvider = p.notificationProvider;
    }
    return *this;
}

void ProviderStore::setAuthenticationProvider(AuthenticationProvider *provider) {
    authenticationProvider = provider;
}

AuthenticationProvider *ProviderStore::getAuthenticationProvider() const {
    return authenticationProvider;
}

void ProviderStore::setNotificationProviders(NotificationProvider *provider) {
    notificationProvider = provider;
}

NotificationProvider *ProviderStore::getNotificationProvider() const {
    return notificationProvider;
}
