#ifndef ENTITY_H
#define ENTITY_H

#include <string>

/**
 * Base class for anything that has an ID.
 */
class Entity {
public:
    explicit Entity(const std::string &id);

    virtual ~Entity() = default;

    std::string getID() const;

private:
    const std::string id;
};

#endif //ENTITY_H
