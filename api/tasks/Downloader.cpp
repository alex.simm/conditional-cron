#include "Downloader.h"
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <sstream>

using namespace std;
using namespace curlpp;
using namespace curlpp::options;

Downloader::Downloader(const string &url) : url(url), followRedirects(false) {
    headers.push_back("Content-Type: application/json");
    headers.push_back("User-Agent: curl/7.77.7");
}

std::string Downloader::getURL() const {
    return url;
}

std::string Downloader::getMethod() const {
    return method;
}

void Downloader::setMethod(const std::string &httpMethod) {
    method = httpMethod;
}

const std::pair<std::string, std::string> &Downloader::getAuthentication() const {
    return authentication;
}

void Downloader::setAuthentication(const std::pair<std::string, std::string> &auth) {
    authentication = auth;
}

bool Downloader::isFollowingRedirects() const {
    return followRedirects;
}

void Downloader::setFollowRedirects(bool follow) {
    followRedirects = follow;
}

std::string Downloader::perform() {
    std::ostringstream os;
    perform(os);
    return os.str();
}

void Downloader::perform(std::ostream &out) {
    // create request, set url and method
    Easy request;
    request.setOpt(new Url(url));
    if (!method.empty())
        request.setOpt(new CustomRequest(method));

    // set HTTP headers and method
    request.setOpt(new HttpHeader(headers));

    // add authentication
    if (!authentication.first.empty()) {
        request.setOpt(new HttpAuth(CURLAUTH_BASIC));
        request.setOpt(new UserPwd(authentication.first + ':' + authentication.second));
    }

    // set other options
    request.setOpt(new SslEngineDefault());
    request.setOpt(new FollowLocation(followRedirects));

    // send request
    request.setOpt(WriteStream(&out));
    request.perform();
}