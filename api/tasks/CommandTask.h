#ifndef COMMANDTASK_H
#define COMMANDTASK_H

#include "../Task.h"
#include <vector>

/**
 * Executes a list of commands in a given order.
 */
class CommandTask : public Task {
public:
    CommandTask(const std::string &id, const std::vector<std::string> &commands);

    std::string getDescription() const override;

    std::vector<std::string> getCommands() const;

    void run(const ProviderStore &providers) override;

private:
    const std::vector<std::string> commands;
};

#endif //COMMANDTASK_H
