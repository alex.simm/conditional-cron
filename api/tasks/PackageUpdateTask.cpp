#include "PackageUpdateTask.h"
#include "../../utils/SystemInfo.h"
#include "../../utils/Logger.h"
#include "../../utils/Utils.h"

using namespace std;
using namespace std::chrono;

PackageUpdateTask::PackageUpdateTask(const string &id) : Task(id) {
}

std::string PackageUpdateTask::getDescription() const {
    return "updating system packages";
}

void PackageUpdateTask::run(const ProviderStore &providers) {
    switch (SystemInfo::getInstance().getPackageManager()) {
        case SystemInfo::PackageManager::DNF: {
            string output = Utils::trim(Utils::runAndReadOutput("dnf check-update -q | wc -l"));
            int number = atoi(Utils::trim(output).c_str());
            if (number > 0) {
                Logger::info("package-updates-task: updating " + to_string(number) + " packages");
                if (providers.getNotificationProvider())
                    providers.getNotificationProvider()->notify(NotificationProvider::NotificationType::INFO, "Updates",
                                                                "Updating " + to_string(number) + " packages");

                system("sudo dnf -y update");
            }
            break;
        }
        case SystemInfo::PackageManager::APT: {
            Logger::info("package-updates-task: updating packages");
            system("sudo apt-get update");
            system("sudo apt-get -y upgrade");
            break;
        }
        default:
            break;
    }
}
