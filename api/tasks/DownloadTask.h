#ifndef DOWNLOADTASK_H
#define DOWNLOADTASK_H

#include "../Task.h"
#include "Downloader.h"
#include <filesystem>
#include <map>

class DownloadTask : public Task {
public:
    DownloadTask(const std::string &id, const std::string &url, const std::string &destination,
                 std::string user = "", std::string authKey = "");

    std::string getDescription() const override;

    void run(const ProviderStore &providers) override;

    void setHTTPOptions(const std::map<std::string, std::string> &options);

    void setCompress(bool compress);

private:
    Downloader downloader;
    const std::filesystem::path destination;
    const std::string user, authKey;
    bool compressDestination;
};

#endif //DOWNLOADTASK_H
