#ifndef TESTTASK_H
#define TESTTASK_H

#include "../Task.h"

class TestTask : public Task {
public:
    TestTask(const std::string &id, int sleepSeconds);

    std::string getDescription() const override;

    void run(const ProviderStore &providers) override;

private:
    const int sleepSeconds;
};

#endif //TESTTASK_H
