#ifndef TASKUTILS_H
#define TASKUTILS_H

#include <string>
#include <nlohmann/json.hpp>
#include <utility>

class TaskUtils {
public:
    static std::string escapeURL(const std::string &url);

    static nlohmann::json selectJSON(const nlohmann::json &root, const std::vector<std::string> &path);
};

#endif //TASKUTILS_H
