#include "TestTask.h"
#include <chrono>
#include <thread>

using namespace std;
using namespace std::chrono;

TestTask::TestTask(const std::string &id, int sleepSeconds) : Task(id), sleepSeconds(sleepSeconds) {
}

std::string TestTask::getDescription() const {
    if (sleepSeconds > 0)
        return "test task (sleep " + to_string(sleepSeconds) + " sec.";
    else
        return "test task";
}

void TestTask::run(const ProviderStore &providers) {
    if (sleepSeconds > 0)
        this_thread::sleep_for(seconds(sleepSeconds));
}
