#include "DownloadTask.h"
#include "TaskUtils.h"
#include <cstdio>
#include <fstream>
#include <filesystem>
#include "../../utils/Logger.h"
#include "../../utils/Utils.h"

using namespace std;
using namespace std::filesystem;

DownloadTask::DownloadTask(const string &id, const string &url, const string &destination, string user, string authKey)
        : Task(id), downloader(url), destination(destination), compressDestination(false), user(user),
          authKey(authKey) {
}

string DownloadTask::getDescription() const {
    return "Download " + downloader.getURL() + " to " + destination.string();
}

void DownloadTask::setCompress(bool compress) {
    this->compressDestination = compress;
}

void DownloadTask::setHTTPOptions(const std::map<std::string, std::string> &options) {
    for (const auto &option : options) {
        if (option.first == "method")
            downloader.setMethod(option.second);
    }
}

void DownloadTask::run(const ProviderStore &providers) {
    // find password
    if (!user.empty()) {
        if (!authKey.empty()) {
            if (providers.getAuthenticationProvider()) {
                string pass = providers.getAuthenticationProvider()->getPassword(authKey);
                downloader.setAuthentication({user, pass});
            }
        }
    }

    // download to temp file
    path tempFile = Utils::createTempFile();
    create_directories(tempFile.parent_path());
    Logger::debug("DownloadTask: downloading " + downloader.getURL() + " to temp file " + tempFile.string());
    ofstream out(tempFile, std::ios_base::trunc | std::ios_base::out);
    downloader.perform(out);
    out.close();

    // compressDestination if necessary
    path dst = destination;
    if (compressDestination) {
        string cmd = "bzip2 -z --best \"" + tempFile.string() + "\"";
        int err = system(cmd.c_str());
        if (err != 0)
            Logger::warning("DownloadTask: could not compressDestination downloaded file " + tempFile.string()
                            + " (destination: " + dst.string() + "): error=" + to_string(err));

        tempFile += ".bz2";
        dst += ".bz2";
    }

    // move to destination
    create_directories(dst.parent_path());
    copy_file(tempFile, dst, copy_options::overwrite_existing);
    Logger::info("successfully downloaded " + downloader.getURL() + " to " + dst.string() + " ("
                 + to_string(file_size(dst)) + " bytes)");

    // cleanup
    remove(tempFile);
}
