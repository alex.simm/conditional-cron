#ifndef SYSTEMUPGRADETASK_H
#define SYSTEMUPGRADETASK_H

#include "../Task.h"
#include "../../utils/VersionNumber.h"
#include "../../utils/SystemInfo.h"

class SystemUpgradeTask : public Task {
public:
    explicit SystemUpgradeTask(const std::string &id);

    std::string getDescription() const override;

    void run(const ProviderStore &providers) override;

private:
    static std::vector<VersionNumber> queryVersionNumbers(SystemInfo::Distribution distribution);

    static int getWikidataEntryNumber(SystemInfo::Distribution distribution);
};

#endif //SYSTEMUPGRADETASK_H
