#ifndef PACKAGEUPDATETASK_H
#define PACKAGEUPDATETASK_H

#include "../Task.h"

class PackageUpdateTask : public Task {
public:
    explicit PackageUpdateTask(const std::string &id);

    std::string getDescription() const override;

    void run(const ProviderStore &providers) override;
};

#endif //PACKAGEUPDATETASK_H
