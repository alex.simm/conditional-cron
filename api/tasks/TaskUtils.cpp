#include "TaskUtils.h"
#include "../../utils/Utils.h"
#include <curlpp/cURLpp.hpp>
#include <sstream>

using namespace std;
using namespace curlpp;
using namespace nlohmann;

std::string TaskUtils::escapeURL(const std::string &url) {
    return escape(url);
}

json TaskUtils::selectJSON(const json &root, const vector<std::string> &path) {
    json current = root;
    for (string s : path) {
        if (current.contains(s))
            current = current[s];
        else
            throw invalid_argument("JSON does not contain the specified path");
    }
    return current;
}