#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <string>
#include <curlpp/Easy.hpp>
#include <list>
#include <utility>

class Downloader {
public:
    explicit Downloader(const std::string &url);

    std::string getURL() const;

    std::string getMethod() const;

    void setMethod(const std::string &httpMethod);

    const std::pair<std::string, std::string> &getAuthentication() const;

    void setAuthentication(const std::pair<std::string, std::string> &auth);

    bool isFollowingRedirects() const;

    void setFollowRedirects(bool follow);

    /**
     * Requests a file using HTTP, stores it in memory, and returns it as a string.
     */
    std::string perform();

    /**
     * Requests a file using HTTP and writes it to the output stream.
     */
    void perform(std::ostream &out);

private:
    std::string url;
    std::string method;
    std::list<std::string> headers;
    std::pair<std::string, std::string> authentication;
    bool followRedirects;
};

#endif //DOWNLOADER_H
