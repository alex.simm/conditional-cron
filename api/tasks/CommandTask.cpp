#include "CommandTask.h"
#include "../../utils/Utils.h"
#include "../../utils/Logger.h"

using namespace std;
using namespace std::chrono;

CommandTask::CommandTask(const string &id, const vector<string> &commands)
        : Task(id), commands(commands) {
}

string CommandTask::getDescription() const {
    return "command task: " + Utils::toString(commands, ",", "'");
}

vector<string> CommandTask::getCommands() const {
    return commands;
}

void CommandTask::run(const ProviderStore &providers) {
    for (const string &cmd : commands) {
        int val = system(cmd.c_str());
        Logger::debug("command '" + cmd + "' returned value " + to_string(val));
    }
}
