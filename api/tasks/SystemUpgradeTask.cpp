#include "SystemUpgradeTask.h"
#include "../../utils/Logger.h"
#include "../../utils/Utils.h"
#include "TaskUtils.h"
#include "Downloader.h"
#include <nlohmann/json.hpp>

using namespace std;
using namespace nlohmann;

SystemUpgradeTask::SystemUpgradeTask(const string &id) : Task(id) {
}

std::string SystemUpgradeTask::getDescription() const {
    return "notification for available system upgrades";
}

void SystemUpgradeTask::run(const ProviderStore &providers) {
    VersionNumber current = SystemInfo::getInstance().getDistributionVersion();
    if (!current.isValid()) {
        Logger::error("system-upgrade-task: could not find valid system version");
        if (providers.getNotificationProvider())
            providers.getNotificationProvider()->notify(NotificationProvider::NotificationType::ERROR, "System upgrade",
                                                        "Could not find valid system version");
    }
    vector<VersionNumber> active = queryVersionNumbers(SystemInfo::getInstance().getDistribution());

    vector<string> strings = Utils::applyTo<string>(active, [](const VersionNumber &v) { return v.toString(); });
    Logger::debug("system-upgrade-task: available versions: "
                  + Utils::append(strings, ",") + " current version: " + current.toString());

    if (!active.empty()) {
        std::sort(active.begin(), active.end());
        if (active[0] > current) {
            Logger::debug("system-upgrade-task: new version available: " + active[0].toString()
                          + " > " + current.toString());

            if (providers.getNotificationProvider())
                providers.getNotificationProvider()->notify(NotificationProvider::NotificationType::INFO,
                                                            "System upgrade",
                                                            "System upgrade available to version: " +
                                                            active[0].toString());
        } else
            Logger::debug("system-upgrade-task: no new versions available");
    }
}

std::vector<VersionNumber> SystemUpgradeTask::queryVersionNumbers(SystemInfo::Distribution distribution) {
    int id = getWikidataEntryNumber(distribution);
    if (id < 0) {
        Logger::error("system-upgrade-task: invalid wikidata entry number '" + to_string(id)
                      + "' for distribution '" + to_string((int) distribution) + "'");
    }

    const string query = "SELECT ?version WHERE { wd:Q" + to_string(id) + " wdt:P348+ ?version; }";
    const string url = "https://query.wikidata.org/sparql?format=json&query=" + TaskUtils::escapeURL(query);

    vector<string> versions;
    try {
        Downloader downloader(url);
        string response = downloader.perform();
        json respJson = json::parse(response);
        json bindings = TaskUtils::selectJSON(respJson, {"results", "bindings"});
        for (json entry : bindings) {
            if (entry.contains("version") && entry["version"].contains("value")) {
                json value = entry["version"]["value"];
                if (value.is_string())
                    versions.push_back(value);
            }
        }
    }
    catch (exception &e) {
        Logger::error("system-upgrade-task: could not request or parse JSON from wikidata");
        return {};
    }

    return Utils::applyTo<VersionNumber>(versions, [](const string &x) { return VersionNumber(x); });
}

int SystemUpgradeTask::getWikidataEntryNumber(SystemInfo::Distribution distribution) {
    switch (distribution) {
        case SystemInfo::Distribution::FEDORA:
            return 48267;
        case SystemInfo::Distribution::UBUNTU:
            return 381;
        case SystemInfo::Distribution::ARCH_LINUX:
            return 185576;
        default:
            return -1;
    }
}
