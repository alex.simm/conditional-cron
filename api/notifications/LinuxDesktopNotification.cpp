#include <future>
#include "LinuxDesktopNotification.h"
#include <functional>
#include <iostream>

using namespace std;

LinuxDesktopNotification::LinuxDesktopNotification() : NotificationProvider("linux-desktop-notification") {
    application = Gio::Application::create("app.conditional-cron", Gio::APPLICATION_FLAGS_NONE);
    application->register_application();
}

void LinuxDesktopNotification::notify(NotificationType type, std::string title, std::string message) {
    auto notification = Gio::Notification::create(title);
    notification->set_body(message);

    string iconName = getIconName(type);
    if (!iconName.empty())
        notification->set_icon(Gio::ThemedIcon::create(iconName));

    application->send_notification(notification);
}

std::string LinuxDesktopNotification::getIconName(NotificationType type) {
    switch (type) {
        case NotificationType::INFO:
            return "dialog-information";
        case NotificationType::WARNING:
            return "dialog-warning";
        case NotificationType::ERROR:
            return "dialog-error";
        case NotificationType::QUESTION:
            return "dialog-question";
        case NotificationType::PASSWORD_REQUEST:
            return "dialog-password";
        default:
            return "";
    }
}