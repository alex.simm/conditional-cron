#ifndef LINUXDESKTOPNOTIFICATION_H
#define LINUXDESKTOPNOTIFICATION_H

#include "../NotificationProvider.h"
#include <giomm.h>
#include <condition_variable>

class LinuxDesktopNotification : public NotificationProvider {
public:
    LinuxDesktopNotification();

    void notify(NotificationType type, std::string title, std::string message) override;

private:
    Glib::RefPtr<Gio::Application> application;

    static std::string getIconName(NotificationType type);
};

#endif //LINUXDESKTOPNOTIFICATION_H
