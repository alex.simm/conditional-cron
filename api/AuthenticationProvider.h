#ifndef KEYCHAIN_H
#define KEYCHAIN_H

#include <string>
#include "Entity.h"

class AuthenticationProvider : public Entity {
public:
    explicit AuthenticationProvider(std::string id) : Entity(id) {
    }

    ~AuthenticationProvider() override = default;

    /**
     * Returns whether this keychain implementation is available on this system.
     */
    virtual bool isAvailable() = 0;

    /**
     * Returns a password for a name.
     */
    virtual std::string getPassword(const std::string &name) = 0;
};

#endif //KEYCHAIN_H
