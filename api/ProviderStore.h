#ifndef IMPLEMENTATIONSTORE_H
#define IMPLEMENTATIONSTORE_H

#include <unordered_set>
#include "Entity.h"
#include "AuthenticationProvider.h"
#include "NotificationProvider.h"

class ProviderStore {
public:
    ProviderStore() = default;

    ProviderStore(const ProviderStore &p);

    ProviderStore &operator=(const ProviderStore &p);

    ~ProviderStore() = default;

    void setAuthenticationProvider(AuthenticationProvider *provider);

    AuthenticationProvider *getAuthenticationProvider() const;

    void setNotificationProviders(NotificationProvider *provider);

    NotificationProvider *getNotificationProvider() const;

private:
    AuthenticationProvider *authenticationProvider = nullptr;
    NotificationProvider *notificationProvider = nullptr;
};

#endif //IMPLEMENTATIONSTORE_H
