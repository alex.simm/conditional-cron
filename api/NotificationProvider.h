#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <string>
#include <vector>
#include "Entity.h"

/**
 * Represents a way to notify a user.
 */
class NotificationProvider : public Entity {
public:
    enum class NotificationType {
        INFO,
        WARNING,
        ERROR,
        QUESTION,
        PASSWORD_REQUEST
    };

    explicit NotificationProvider(std::string id) : Entity(id) {
    }

    /**
     * Shows a notification.
     */
    virtual void notify(NotificationType type, std::string title, std::string message) = 0;
};

#endif //NOTIFICATION_H
