#ifndef TASK_H
#define TASK_H

#include <vector>
#include <string>
#include <chrono>
#include "Condition.h"
#include "ProviderStore.h"

class Task : public Entity {
public:
    virtual ~Task() = default;

    std::string toString() const;

    const std::vector<Condition *> &getConditions() const;

    void setConditions(const std::vector<Condition *> &cond);

    virtual std::string getDescription() const = 0;

    std::chrono::seconds getUpdateInterval(bool success) const;

    void setUpdateInterval(std::chrono::seconds interval, bool success);

    virtual void run(const ProviderStore &providers) = 0;

protected:
    std::vector<Condition *> conditions;
    std::chrono::seconds updateIntervalSuccess, updateIntervalFailure;

    explicit Task(const std::string &id);
};

#endif //TASK_H
