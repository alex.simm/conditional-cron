#include "GnomeKeyring.h"
#include <glib.h>
#include <stdexcept>
#include "libsecret/secret.h"
#include "../../utils/Logger.h"

using namespace std;

GnomeKeyring::GnomeKeyring() : AuthenticationProvider("gnome-keyring") {
}

bool GnomeKeyring::isAvailable() {
    GError *err = nullptr;
    SecretService *service = secret_service_get_sync(SECRET_SERVICE_LOAD_COLLECTIONS, nullptr, &err);
    g_object_unref(service);

    return err == nullptr;
}

//TODO: this is a bad workaround and should use proper methods of libsecret
string GnomeKeyring::getPassword(const string &name) {
    // load service
    GError *err = nullptr;
    SecretService *service = secret_service_get_sync(SECRET_SERVICE_LOAD_COLLECTIONS, nullptr, &err);
    if (err != nullptr)
        throw runtime_error(formatError(err));

    string *found = nullptr;

    // find and unlock collections
    GList *collections = secret_service_get_collections(service);
    GList *unlocked;
    Logger::debug("GnomeKeyring: trying to unlock collections");
    secret_service_unlock_sync(service, collections, nullptr, &unlocked, &err);
    if (g_list_length(unlocked) != g_list_length(collections))
        Logger::warning("GnomeKeyring: could not unlock all collections");

    // iterate collections
    while (unlocked != nullptr && found == nullptr) {
        auto col = static_cast<SecretCollection *>(unlocked->data);

        // iterate items in collection
        GList *items = secret_collection_get_items(col);
        while (items != nullptr && found == nullptr) {
            auto item = static_cast<SecretItem *>(items->data);

            // check item
            gchar *label = secret_item_get_label(item);
            if (string(label) == name)
                found = getSecret(item);

            g_free(label);
            g_object_unref(item);
            items = items->next;
        }
        g_list_free(items);

        g_object_unref(col);
        unlocked = unlocked->next;
    }

    // clean
    g_list_free(collections);
    g_list_free(unlocked);
    g_object_unref(service);

    if (found == nullptr)
        throw runtime_error("No key with the name '" + name + "' found");

    string copy(*found);
    delete found;
    return copy;
}

string *GnomeKeyring::getSecret(SecretItem *item) {
    GError *err = nullptr;
    secret_item_load_secret_sync(item, nullptr, &err);
    if (err != nullptr)
        throw runtime_error(formatError(err));

    SecretValue *val = secret_item_get_secret(item);
    if (val != nullptr) {
        gsize len;
        const gchar *value = secret_value_get(val, &len);
        auto out = new string(value, len);
        secret_value_unref(val);
        return out;
    } else {
        return nullptr;
    }
}

string GnomeKeyring::formatError(GError *err) {
    if (err == nullptr)
        return "";

    string text;
    if (err->domain == SECRET_ERROR) {
        switch (err->code) {
            case SECRET_ERROR_PROTOCOL:
                text = "Error: Recieived invalid data from secret Service";
                break;
            case SECRET_ERROR_IS_LOCKED:
                text = "Error: Secret item or collection is locked";
                break;
            case SECRET_ERROR_NO_SUCH_OBJECT:
                text = "Error: Secret item or collection not found";
                break;
            case SECRET_ERROR_ALREADY_EXISTS:
                text = "Error: Secret item or collection already exists";
                break;
            default:
                text = "";
                break;
        }
    } else {
        text = "Error: Couldn't get secret service for unkown reason";
    }

    return text + ": " + err->message;
}
