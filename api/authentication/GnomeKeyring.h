#ifndef GNOMEKEYCHAIN_H
#define GNOMEKEYCHAIN_H

#include <libsecret/secret.h>
#include "../AuthenticationProvider.h"

class GnomeKeyring : public AuthenticationProvider {
public:
public:
    GnomeKeyring();

    ~GnomeKeyring() override = default;

    bool isAvailable() override;

    std::string getPassword(const std::string &name) override;

private:
    static std::string *getSecret(SecretItem *item);

    static std::string formatError(GError *err);
};

#endif //GNOMEKEYCHAIN_H
