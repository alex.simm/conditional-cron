#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <string>
#include <vector>
#include <chrono>
#include <nlohmann/json.hpp>
#include "api/NotificationProvider.h"
#include "api/Task.h"
#include "api/AuthenticationProvider.h"
#include "Scheduler.h"

class ConfigParser {
public:
    static NotificationProvider *parseNotificationProvider(const nlohmann::json &config);

    static AuthenticationProvider *parseAuthenticationProvider(const nlohmann::json &config);

    static std::chrono::seconds parseInterval(const nlohmann::json &config);

    static Condition *parseCondition(const std::string &id, const nlohmann::json &config);

    static std::map<std::string, Condition *> parseConditions(const nlohmann::json &config);

    static Task *parseTask(const std::string &id, const nlohmann::json &config,
                           const std::map<std::string, Condition *> &conditions);

    static std::vector<Task *> parseTasks(const nlohmann::json &config,
                                          const std::map<std::string, Condition *> &conditions);

    static void *createGUI(const nlohmann::json &config, Scheduler &scheduler);

private:
    static std::vector<Condition *> findTaskConditions(std::map<std::string, Condition *> conditions,
                                                       const nlohmann::json &conditionIds);
};

#endif //CONFIGPARSER_H
